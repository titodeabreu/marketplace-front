# Marketplace-front - An Assessment for Altran/Natixis

# Run Locally


##### Pre-requisite
 - docker installed

### Execute docker build:
    docker build -f DockerFile -t marketplace-front .
    
### Create container by image
    docker run --name marketplace-front -p 4200:4200 marketplace-front