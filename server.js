var express = require('express');
var app = express();
var cors = require('cors');
var http = require('http');
var bodyParser = require('body-parser');
var PORT = 4200;
//CORS Middleware
app.use(cors());
app.options('*', cors());
app.use(function (req, res, next) {
//Enabling CORS
res.header('Access-Control-Allow-Origin', '*');
res.header('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, x-client-key, x-client-token, x-client-secret, Authorization');
next();
});
//files configure
app.use(express.static(__dirname + '/dist/marketplace-front'));
//body parse
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ limit: '50mb',extended: true }))
// parse application/json
app.use(bodyParser.json({limit: '50mb', extended: true}))

app.listen(process.env.PORT || 4200);