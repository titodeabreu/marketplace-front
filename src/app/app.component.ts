import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'MarketPlace App';

  constructor(private router: Router) {}

  users() {
    this.router.navigate(['/users']);
  }

  items() {
    this.router.navigate(['/items']);
  }

  shoppingCart() {
    this.router.navigate(['/shopping-cart']);
  }

}
