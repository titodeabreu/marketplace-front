import { Item } from './item';

export class Sale {
    itemDTO: Item;
    quantity: number;
  }
