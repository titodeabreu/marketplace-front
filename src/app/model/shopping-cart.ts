import { Sale } from './sale';

export class ShoppingCartDTO {
    id: string;
    userId: string;
    shoppingCartOpen: boolean;
    sales: Sale[];
    total: number;
  }
