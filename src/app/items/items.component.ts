import { Component, OnInit } from '@angular/core';
import { ItemApiService } from '../service/item-api.service';
import { Item } from '../model/item';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {
  
  displayedColumns: string[] = ['name', 'value'];
  data: Item[] = [];
  isLoadingResults = true;

  constructor(private api: ItemApiService) { }

  ngOnInit() {
    this.api.getItems()
    .subscribe((res: any) => {
      this.data = res;
      console.log(this.data);
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  }

}
