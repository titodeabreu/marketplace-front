import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ItemApiService } from '../service/item-api.service';
import { Item } from '../model/item';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-item-edit',
  templateUrl: './item-edit.component.html',
  styleUrls: ['./item-edit.component.css']
})
export class ItemEditComponent implements OnInit {

  itemForm: FormGroup;
  id = '';
  name = '';
  value = 0.0;
  isLoadingResults = false;
  matcher = new MyErrorStateMatcher();

  constructor(private router: Router, private route: ActivatedRoute, private api: ItemApiService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getItem(this.route.snapshot.params['id']);
    this.itemForm = this.formBuilder.group({
      'name': [null, Validators.required],
      'value': [null, Validators.required]
    });
  }

  getItem(id: any) {
    this.api.getItem(id).subscribe((data: Item) => {
      this.id = data.id;
      this.itemForm.setValue({
        name: data.name,
        value: data.value
      });
    });
  }

  onFormSubmit() {
    this.isLoadingResults = true;
    const item: Item =  this.itemForm.value;
    item.id = this.id;
    this.api.updateItem(this.id, item)
      .subscribe((res: Item) => {
          const id = res.id;
          this.isLoadingResults = false;
          this.router.navigate(['/item-details', id]);
        }, (err: any) => {
          console.log(err);
          this.isLoadingResults = false;
        }
      );
  }

  itemDetails() {
    this.router.navigate(['/item-details', this.id]);
  }

}
