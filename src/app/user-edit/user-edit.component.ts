import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserApiService } from '../service/user-api.service';
import { User } from '../model/user';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {

  userForm: FormGroup;
  id = '';
  name = '';
  mail = '';
  isLoadingResults = false;
  matcher = new MyErrorStateMatcher();

  constructor(private router: Router, private route: ActivatedRoute, private api: UserApiService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getUser(this.route.snapshot.params['id']);
    this.userForm = this.formBuilder.group({
      'name': [null, Validators.required],
      'mail': [null, Validators.required]
    });
  }

  getUser(id: any) {
    this.api.getUser(id).subscribe((data: User) => {
      this.id = data.id;
      this.userForm.setValue({
        name: data.name,
        mail: data.mail
      });
    });
  }

  onFormSubmit() {
    this.isLoadingResults = true;
    const user: User =  this.userForm.value;
    user.id = this.id;
    this.api.updateUser(this.id, user)
      .subscribe((res: User) => {
          const id = res.id;
          this.isLoadingResults = false;
          this.router.navigate(['/user-details', id]);
        }, (err: any) => {
          console.log(err);
          this.isLoadingResults = false;
        }
      );
  }

  userDetails() {
    this.router.navigate(['/user-details', this.id]);
  }

}
