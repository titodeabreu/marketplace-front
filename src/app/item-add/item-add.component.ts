import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ItemApiService } from '../service/item-api.service';
import { Item } from '../model/item';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-item-add',
  templateUrl: './item-add.component.html',
  styleUrls: ['./item-add.component.css']
})
export class ItemAddComponent implements OnInit {

  itemForm: FormGroup;
  name = '';
  value = 0.0;
  isLoadingResults = false;
  matcher = new MyErrorStateMatcher();

  constructor(private router: Router, private api: ItemApiService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.itemForm = this.formBuilder.group({
      'name' : [null, Validators.required],
      'value' : [null, Validators.required]
    });
  }

  onFormSubmit() {
    this.isLoadingResults = true;
    this.api.addItem(this.itemForm.value)
      .subscribe((res: any) => {
          this.isLoadingResults = false;
          const id = res.id;
          this.router.navigate(['/item-details', id]);
        }, (err: any) => {
          console.log(err);
          alert(err.error.message);
          this.isLoadingResults = false;
        });
  }

}
