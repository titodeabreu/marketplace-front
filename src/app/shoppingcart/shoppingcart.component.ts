import { Component, OnInit } from '@angular/core';
import { UserApiService } from '../service/user-api.service';
import { ItemApiService } from '../service/item-api.service';
import { ShoppingcartApiService } from '../service/shoppingcart-api.service';
import { User } from '../model/user';
import { ShoppingCartDTO } from '../model/shopping-cart';
import { Item } from '../model/item';

@Component({
  selector: 'app-shoppingcart',
  templateUrl: './shoppingcart.component.html',
  styleUrls: ['./shoppingcart.component.css']
})
export class ShoppingcartComponent implements OnInit {
  displayedColumns: string[] = ['name', 'value', 'quantity'];

  isLoadingResults = true;
  users: User[] = [];
  items: Item[] = [];
  selectedUser: User;
  selectedItem: Item;
  shoppingCart: ShoppingCartDTO;

  constructor(private itemApi: ItemApiService, private userApi: UserApiService, private shoppingcartApi: ShoppingcartApiService) {
    this.selectedUser = new User();
    this.selectedItem = new Item();
   }

  ngOnInit() {
    this.retrieveAllUsers();
    this.retrieveAllItems();
  }

  cleanUpItem() {
    this.selectedItem = null;
    this.retrieveAllItems();
  }

  clearShoppingCart() {
    this.isLoadingResults = true;
    this.shoppingcartApi.cleanCart(this.shoppingCart.id)
    .subscribe((res: any) => {
      this.retrieveShoppingCart();
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  }
  deleteItemFromShoppingCart(itemId: any) {
    this.isLoadingResults = true;
    this.shoppingcartApi.deleteItemFromCart(this.shoppingCart.id, itemId)
    .subscribe((res: any) => {
      this.retrieveShoppingCart();
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  }

  selectingUser() {
    this.createShoppingCart();
  }

  retrieveShoppingCart() {
    this.isLoadingResults = true;
    return this.shoppingcartApi.getShoppingCart(this.shoppingCart.id)
    .subscribe((res: ShoppingCartDTO) => {
      this.shoppingCart = res;
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  }

  closeShoppingCart() {
    this.isLoadingResults = true;
    this.shoppingcartApi.closeShoppingCart(this.shoppingCart.id)
    .subscribe((res: any) => {
      this.shoppingCart = res;
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  }

  deleteShoppingCart() {
    this.isLoadingResults = true;
    this.shoppingcartApi.deleteShoppingCart(this.shoppingCart.id)
    .subscribe((res: any) => {
      this.retrieveShoppingCart();
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  }

  addItem(itemId: any) {
    this.isLoadingResults = true;
    this.shoppingcartApi.addItemIntoCart(this.shoppingCart.id, itemId)
    .subscribe((res: any) => {
      this.isLoadingResults = false;
      this.cleanUpItem();
      this.retrieveShoppingCart();
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  }

  createShoppingCart() {
    this.isLoadingResults = true;
    this.shoppingcartApi.createShoppingCart(this.selectedUser.id)
    .subscribe((res: any) => {
      this.shoppingCart = res;
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  }

  retrieveAllUsers() {
    this.isLoadingResults = true;
    this.userApi.getUsers()
    .subscribe((res: any) => {
      this.users = res;
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  }

  retrieveAllItems() {
    this.isLoadingResults = true;
    this.itemApi.getItems()
    .subscribe((res: any) => {
      this.items = res;
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  }

}