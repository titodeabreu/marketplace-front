import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ItemApiService } from '../service/item-api.service';
import { Item } from '../model/item';

@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.css']
})
export class ItemDetailComponent implements OnInit {
  item: Item = new Item();
  isLoadingResults = true;

  constructor(private api: ItemApiService, private route: ActivatedRoute, private router: Router) {
   }

  ngOnInit() {
    this.getItemDetails(this.route.snapshot.params['id']);
  }

  deleteItem(id: any) {
    this.isLoadingResults = true;
    this.api.deleteItem(id)
      .subscribe(res => {
        this.isLoadingResults = false;
        this.router.navigate(['/items']);
      }, (err) => {
        console.log(err);
        this.isLoadingResults = false;
      }
      );
  }

  getItemDetails(id: any) {
    this.api.getItem(id)
      .subscribe((data: any) => {
        this.item = data;
        console.log(this.item);
        this.isLoadingResults = false;
      });
    }
}
