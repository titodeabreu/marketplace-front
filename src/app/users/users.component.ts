import { Component, OnInit } from '@angular/core';
import { UserApiService } from '../service/user-api.service';
import { User } from '../model/user';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  displayedColumns: string[] = ['name', 'mail'];
  data: User[] = [];
  isLoadingResults = true;

  constructor(private api: UserApiService) { }

  ngOnInit() {
    this.api.getUsers()
    .subscribe((res: any) => {
      this.data = res;
      console.log(this.data);
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  }

}
