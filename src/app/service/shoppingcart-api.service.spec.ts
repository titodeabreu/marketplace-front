import { TestBed } from '@angular/core/testing';

import { ShoppingcartApiService } from './shoppingcart-api.service';

describe('ShoppingcartApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ShoppingcartApiService = TestBed.get(ShoppingcartApiService);
    expect(service).toBeTruthy();
  });
});
