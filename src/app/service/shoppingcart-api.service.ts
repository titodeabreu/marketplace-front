import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import {environment} from 'src/environments/environment';
import { ShoppingCartDTO } from '../model/shopping-cart';


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json',
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Methods': 'POST, GET, PUT, DELETE, OPTIONS',
  'Access-Control-Allow-Headers': 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept'})
};
const apiUrl = environment.baseUrl + '/api/sc';


@Injectable({
  providedIn: 'root'
})
export class ShoppingcartApiService {

  constructor(private http: HttpClient) { }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }

  createShoppingCart(id: any): Observable<ShoppingCartDTO> {
    const url = `${apiUrl}/${id}`;
    return this.http.post(url, {}, httpOptions).pipe(
      tap((cart: ShoppingCartDTO) => console.log(`added cart w/ id=${cart.id}`))
    );
  }

  addItemIntoCart(scId: any, itemId: any): Observable<ShoppingCartDTO> {
    const url = `${apiUrl}/${scId}/${itemId}`;
    return this.http.post(url, {}, httpOptions).pipe(
      tap((cart: ShoppingCartDTO) => console.log(`added sale w/ sales=${cart.sales}`))
    );
  }

  getShoppingCart(id: any): Observable<ShoppingCartDTO> {
    const url = `${apiUrl}/${id}`;
    return this.http.get<ShoppingCartDTO>(url).pipe(
      tap(_ => console.log(`fetched shopping cart id=${id}`))
    );
  }

  deleteShoppingCart(id: any): Observable<ShoppingCartDTO> {
    const url = `${apiUrl}/cart/${id}`;
    return this.http.delete<ShoppingCartDTO>(url, httpOptions).pipe(
      tap(_ => console.log(`deleted shopping cart id=${id}`)),
      catchError(this.handleError<ShoppingCartDTO>('shopping cart'))
    );
  }

  deleteItemFromCart(scId: any, itemId: any): Observable<ShoppingCartDTO> {
    const url = `${apiUrl}/${scId}/${itemId}`;
    return this.http.delete<ShoppingCartDTO>(url, httpOptions).pipe(
      tap(_ => console.log(`deleted item id=${itemId} from shopping cart id=${scId}`)),
      catchError(this.handleError<ShoppingCartDTO>('item from shopping cart'))
    );
  }

  cleanCart(id: any): Observable<ShoppingCartDTO> {
    const url = `${apiUrl}/clean/${id}`;
    return this.http.delete<ShoppingCartDTO>(url, httpOptions).pipe(
      tap(_ => console.log(`clean shopping cart id=${id}`)),
      catchError(this.handleError<ShoppingCartDTO>('clean shopping cart'))
    );
  }

  closeShoppingCart(id: any): Observable<any> {
    const url = `${apiUrl}/${id}`;
    return this.http.put(url, {}, httpOptions).pipe(
      tap(_ => console.log(`close shopping cart with id=${id}`))
    );
  }

}
