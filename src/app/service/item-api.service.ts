import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Item } from '../model/item';
import {environment} from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl = environment.baseUrl+'/api/items';

@Injectable({
  providedIn: 'root'
})
export class ItemApiService {

  constructor(private http: HttpClient) { }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }

  getItems(): Observable<Item[]> {
    return this.http.get<Item[]>(apiUrl)
      .pipe(
        tap(item => console.log('fetched items')),
        catchError(this.handleError('getItems', []))
      );
  }

  getItem(id: any): Observable<Item> {
    const url = `${apiUrl}/${id}`;
    return this.http.get<Item>(url).pipe(
      tap(_ => console.log(`fetched item id=${id}`))
    );
  }

  addItem(item: Item): Observable<Item> {
    return this.http.post<Item>(apiUrl, item, httpOptions).pipe(
      tap((u: Item) => console.log(`added item w/ id=${item.id}`))
    );
  }

  updateItem(id: any, item: Item): Observable<any> {
    const url = `${apiUrl}/${id}`;
    return this.http.put(url, item, httpOptions).pipe(
      tap(_ => console.log(`updated item id=${id}`))
    );
  }

  deleteItem(id: any): Observable<Item> {
    const url = `${apiUrl}/${id}`;
    return this.http.delete<Item>(url, httpOptions).pipe(
      tap(_ => console.log(`deleted item id=${id}`)),
      catchError(this.handleError<Item>('deleteItem'))
    );
  }
}
